import 'package:flutter/material.dart';
import './pages/map_ant.dart';
import './pages/course_selection.dart';
import './pages/results.dart';
import './pages/show_results.dart';
import './pages/home.dart';
import 'package:flutter/services.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:permission_handler/permission_handler.dart';

//import 'package:wakelock/wakelock.dart';

//void main() => runApp(MyApp());
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Map<Permission, PermissionStatus> statuses = await [
    Permission.location,
    Permission.storage,
  ].request();
  print(statuses[Permission.location]);
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //Wakelock.enable();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        initialRoute: HomePage.route,
        routes: <String, WidgetBuilder>{
          HomePage.route: (context) => HomePage(),
          //SignInDemo.route: (context) => SignInDemo(),
          CustomCrsPage.route: (context) => CustomCrsPage(),
          CourseSelection.route: (context) => CourseSelection(),
          Results.route: (context) => Results(),
          ShowResults.route: (context) => ShowResults(),

        });
    // home: CustomCrsPage(),
    //routes: <String, WidgetBuilder>{
    //  CustomCrsPage.route: (context) => CustomCrsPage(),
    //  CourseSelection.route: (context) => CourseSelection(),
    //}
  }
}

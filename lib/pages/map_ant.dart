import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:proj4dart/proj4dart.dart' as proj4;
import 'package:geolocator/geolocator.dart';
import 'dart:async';
import 'package:geodesy/geodesy.dart';
import '../classes/piste.dart';
import '../classes/course.dart';
import '../classes/userdata.dart';

import 'dart:io';

import '../classes/track.dart';

class CustomCrsPage extends StatefulWidget {
  final String courseName;
  final String userName;
  CustomCrsPage({this.courseName = null, this.userName = null});
  static const String route = 'custom_crs';


  @override
  _CustomCrsPageState createState() => _CustomCrsPageState();
}

class _CustomCrsPageState extends State<CustomCrsPage> {
  Geolocator _geolocator;
  Position _position;
  LatLng oldp;
  LatLng newp;
  Piste piste;
  bool found = false;
  double oldRotation = 0.0;
  double rotation = 0.0;
  double pseudoRotation = null;
  double latitude = 60.192059;
  double longitude = 24.945831;
  Proj4Crs epsg3067CRS;
  proj4.Projection epsg3067;
  Position oldPosition = null;
  bool doRotate = true;
  Geodesy geodesy = Geodesy();
  double maxZoom = 17;
  Future<List> futureTrack;
  String initText = 'Map centered to';
  MapController mapController;
  //StatefulMapController statefulMapController;
  //StreamSubscription<StatefulMapControllerStateChange> sub;

  bool ready = false;
  bool doCenter = true;
  var course;
  var savedata = SaveData();

  @override
  void initState() {
    print("ON tapped 2 ");

    mapController = MapController();
    super.initState();

    _geolocator = Geolocator();
    //piste = Piste(lat: 60.192059, lon: 24.945831);
    LocationOptions locationOptions = LocationOptions(
        accuracy: LocationAccuracy.high,
        distanceFilter: 15, //meters
        timeInterval: 2000); //milliseconds
    StreamSubscription positionStream = _geolocator
        .getPositionStream(locationOptions)
        .listen((Position position) {
      oldPosition = _position;
      _position = position;
      latitude = _position.latitude;
      longitude = _position.longitude;
      setState(() {
        piste = Piste(lat: _position.latitude, lon: _position.longitude);
        //do_rotation();
        //mapController.onRotationChanged(10.0);
      });
    });

    // From: http://epsg.io/3067, proj definition: http://epsg.io/3067.proj4
    // Find Projection by name or define it if not exists
    epsg3067 = proj4.Projection('EPSG:3067') ??
        proj4.Projection.add('EPSG:3067',
            '+proj=utm +zone=35 +ellps=GRS80 +units=m +towgs84=0,0,0,-0,-0,-0,0 +no_defs');
    // mapAnt example zoom level resolutions
    final resolutions = <double>[
      8192,
      4096,
      2048,
      1024,
      512,
      256,
      128,
      64,
      32,
      16,
      8,
      4,
      2,
      1,
      0.5,
      0.25
    ];

    // [-548576.0, 6291456.0, 1548576.0, 8388608],
    final epsg3067Bounds = Bounds<double>(
      CustomPoint<double>(-548576.0, 6291456.0),
      CustomPoint<double>(1548576.0, 8388608.000000),
    );

    maxZoom = (resolutions.length - 1).toDouble();

    // Define CRS
    epsg3067CRS = Proj4Crs.fromFactory(
      // CRS code
      code: 'EPSG:3067',
      // your proj4 delegate
      proj4Projection: epsg3067,
      // Resolution factors (projection units per pixel, for example meters/pixel)
      // for zoom levels; specify either scales or resolutions, not both
      resolutions: resolutions,
      // Bounds of the CRS, in projected coordinates
      // (if not specified, the layer's which uses this CRS will be infinite)
      bounds: epsg3067Bounds,
      // Tile origin, in projected coordinates, if set, this overrides the transformation option
      // Some goeserver changes origin based on zoom level
      // and some are not at all (use explicit/implicit null or use [CustomPoint(0, 0)])
      // @see https://github.com/kartena/Proj4Leaflet/pull/171
      origins: [CustomPoint(0, 0)],
      // Scale factors (pixels per projection unit, for example pixels/meter) for zoom levels;
      // specify either scales or resolutions, not both
      scales: null,
      // The transformation to use when transforming projected coordinates into pixel coordinates
      transformation: null,
    );

  }

  void do_rotation() {
    //print("rotate1");
    if (oldPosition != null && _position != null) {
      oldp = LatLng(oldPosition.latitude, oldPosition.longitude);
      newp = LatLng(_position.latitude, _position.longitude);
      //LatLng oldp = LatLng(65.00, 25.00);
      //LatLng newp = LatLng(65.00, 26.00);
      rotation = geodesy.finalBearingBetweenTwoGeoPoints(oldp, newp);
      //print("oldp:" + oldp.toString());
      //print("newpp:" + newp.toString());

      //print("bearing:" + rotation.toString());
      if ((rotation != oldRotation) || (pseudoRotation == 0.0)) {
        //print("rotate2!");
        mapController.rotate(360.0 - rotation);
        oldRotation = rotation;
      }
    }
  }

  LatLng get_point() {
    if (mapController.ready) {
      if (doCenter) {
        if (doRotate) {
          //print("rotating normally");
          do_rotation();
          pseudoRotation = null;
        }
        mapController.move(LatLng(latitude, longitude), mapController.zoom);
      } else {
        // not centering, rotate back to 0 deg
        if (pseudoRotation == null) {
          //print("rotating to zero");
          mapController.rotate(0.0);
          pseudoRotation = 0.0;
        }
      }
    }
    return LatLng(latitude, longitude);
  }


  Future<Position> getLocation() async {
    var currentLocation;
    try {
      currentLocation = await _geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  @override
  Widget build(BuildContext context) {
    final String args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      //appBar: AppBar(title: Text('Custom CRS')),
      //drawer: buildDrawer(context, CustomCrsPage.route),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder<Track>(
          future: fetchTrack(args),
          builder: (context, snapshot) {
            if ((snapshot.hasData) && (piste != null)){
              if (course == null) {
                course = Course(
                    runnerName: savedata.getUser(),
                    courseName: snapshot.data.courseName,
                    controls: snapshot.data.trackList,
                    controlcircles: snapshot.data.controls);
                print("use name:"+savedata.getUser());
              }

              course.check_punch(latLon: piste.currentLatLon);
              return FlutterMap(
                mapController: mapController,
                options: MapOptions(
                    // Set the default CRS
                    crs: epsg3067CRS,
                    center: get_point(),
                    //center: userCenter,
                    zoom: 13,
                    // Set maxZoom usually scales.length - 1 OR resolutions.length - 1
                    // but not greater
                    maxZoom: maxZoom,
                    //maxZoom: 16.0,
                    onTap: (point) {
                      final snackBar = SnackBar(
                        content: Text(
                            "PAINA PITKÄÄN niin vaihtuu: keskitetty/ei keskitetty kartta "),
                        duration: Duration(seconds: 3),
                        backgroundColor: Colors.red,
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                    },
                    onLongPress: (point) {
                      String teksti;
                      doCenter = !doCenter;
                      doRotate = !doRotate;
                      //print("docenter:" + doCenter.toString());
                      //print("dorotate:" + doCenter.toString());
                      if (doRotate) {
                        teksti = "Valittu: Kartan keskitys+pyöritys";
                      } else {
                        teksti = "Valittu: Kartan liikuttelu+skaalaus";
                      }
                      final snackBar = SnackBar(
                        content: Text(teksti),
                        duration: Duration(seconds: 3),
                        backgroundColor: Colors.blue,
                      );
                      Scaffold.of(context).showSnackBar(snackBar);
                      get_point();
                    }),
                layers: [
                  TileLayerOptions(
                    wmsOptions: WMSTileLayerOptions(
                      // Set the WMS layer's CRS too
                      crs: epsg3067CRS,
                      baseUrl: 'http://wms.mapant.fi/wms.php?',
                      layers: ['default'],
                      //baseUrl: 'https://sopimus-karttakuva.maanmittauslaitos.fi/sopimus/service/wms?api-key=1ad24dc4-6b06-4b84-b65e-7fabdfcda10e',
                      //layers: ['default'],
                    ),
                  ),

                  //TileLayerOptions(
                  //    maxNativeZoom: maxZoom,
                  //    urlTemplate:
                  //        //"https://mtb-tileserver.trailmap.fi/tiles/{z}/{x}/{y}.png",
                   //   "https://tiles.kartat.kapsi.fi/peruskartta/{z}/{x}/{y}.jpg",
                   //   //"https://tilestrata.trailmap.fi/d3-mtbmap-cache/3d/{z}/{x}/{y}@2x.webp",
                    //  //"https://mtb-tileserver.trailmap.fi/tiles@2x/{z}/{x}/{y}.png",
                     // //"https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
                     // subdomains: ['a', 'b', 'c']),
                  CircleLayerOptions(
                      //circles: snapshot.data.controls + piste.piste),
                      circles: piste.piste + course.controlcircles
                  ),
                  PolylineLayerOptions(polylines: [
                    Polyline(
                      points: course.controls,
                      // isDotted: true,
                      color: Colors.red,
                      strokeWidth: 3.0,
                      //borderColor: Colors.red,
                      //borderStrokeWidth: 0.1,
                    )
                  ]),
                  //}
                  //MarkerLayerOptions(
                  //  markers: snapshot.data.controls;
                  //      ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}

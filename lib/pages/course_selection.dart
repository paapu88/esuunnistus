import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'dart:convert';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import '../widgets/drawer.dart';
import 'package:permission_handler/permission_handler.dart';
import '../pages/map_ant.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

CollectionReference _collectionRef = FirebaseFirestore.instance.collection('courses');

Future<void> getData() async {
  // Get docs from collection reference
  var mycol = await _collectionRef.get();
  //final courses = mycol.docs.map((doc) => doc.data()).toList();
  final courses = mycol.docs.map((doc) => doc.id).toList();
  print(courses);
  return courses;
}


class CourseSelection extends StatelessWidget {
  static const String route = 'CourseSelection';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Valitse suunnistusrata!"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        var post = snapshot.data[index];
                        return Container(
                            child: Card(
                          child: ListTile(
                            title: Text(post),
                            onTap: () => onTapped(post, context),
                          ),
                        ));
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    print("ON tapped");
    Navigator.pushReplacementNamed(context, CustomCrsPage.route,
        arguments: post);
  }
}

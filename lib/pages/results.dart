import 'package:flutter/material.dart';
import '../pages/show_results.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

CollectionReference _collectionRef = FirebaseFirestore.instance.collection('courses');

Future<void> getData() async {
  // Get docs from collection reference
  var mycol = await _collectionRef.get();
  //final courses = mycol.docs.map((doc) => doc.data()).toList();
  final courses = mycol.docs.map((doc) => doc.id).toList();
  print(courses);
  return courses;
}


class Results extends StatelessWidget {
  static const String route = 'Results';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Valitse tulokset"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder(
          future: getData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        var post = snapshot.data[index];
                        return Container(
                            child: Card(
                          child: ListTile(
                            title: Text(post),
                            onTap: () => onTapped(post, context),
                          ),
                        ));
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    print("ON tapped");
    Navigator.pushReplacementNamed(context, ShowResults.route,
        arguments: post);
  }
}

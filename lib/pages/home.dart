import 'package:flutter/material.dart';
import '../pages/map_ant.dart';
import '../pages/course_selection.dart';
import '../pages/results.dart';
import 'package:google_sign_in/google_sign_in.dart';
import '../classes/userdata.dart';
//import 'package:firebase_auth/firebase_auth.dart';

import '../widgets/drawer.dart';

class HomePage extends StatefulWidget {
  static const String route = '/';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GoogleSignInAccount _currentUser;
  final titles = ['Valitse Suunnistusrata', 'Näytä Kartta', 'Näytä tulokset'];
  var savedata = SaveData();
  //final FirebaseAuth _auth = FirebaseAuth.instance;

  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: <String>[
      'email',
    ],
  );

  Future<void> _handleSignIn() async {
    try {
      await _googleSignIn.signIn();
    } catch (error) {
      print(error);
    }
  }

  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
    });
    print("current user:" + _currentUser.toString());
    _googleSignIn.signInSilently();
  }

  @override
  Widget build(BuildContext context) {
    if (_currentUser != null) {
      return Scaffold(
        appBar: AppBar(
          title: Text(_currentUser.email),
        ),
        body: ListView.builder(
          itemCount: titles.length,
          itemBuilder: (BuildContext context, int index) {
            var post = titles[index];

            return Container(
                child: Card(
                  child: ListTile(
                    title: Text(post),
                    onTap: () => onTapped(post, context),
                  ),
                ));
          },
        ),
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          const Text("You are not currently signed in."),
          RaisedButton(
            child: const Text('SIGN IN'),
            onPressed: _handleSignIn,
          ),
        ],
      );
    }
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    savedata.setUser(this._currentUser.email);
    if (post == titles[0]) {
      Navigator.pushReplacementNamed(context, CourseSelection.route);
    } else if (post == titles[2]){
      Navigator.pushReplacementNamed(context, Results.route);
    }

    else {
      Navigator.pushReplacementNamed(context, CustomCrsPage.route);
    }
  }
}


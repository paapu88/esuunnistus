import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:collection';
import 'package:flutter/material.dart';


CollectionReference _collectionRef = FirebaseFirestore.instance.collection('courses');

String to_min_sec(int s){
  // generate time in minutes and seconds as a float, seconds range from 0 to 59 (not from 0 to 99)
  int min = (s / 60).truncate();
  int sec = (s - min * 60.0).truncate();
  String minsec = min.toString().padLeft(2, '0') + '.' +
      sec.toString().padLeft(2, '0');
  return minsec;
  //return double.parse(minsec);
}

DataTable fetchTulos(Map data)  {
  //track=new Track(courseName);
  final mellanresult = SplayTreeMap<String, double>((a, b) => a.compareTo(b));
  var result = [];
  var runner, times, time;
  var minssecs = [];
  int hour, min, sec;
  double minsec;
  List<DataRow> results = <DataRow>[];
  List<DataColumn> titles = [];

  print("in fetch track");
  print(data);

  var nofcontrols = data['nofcontrols'];
  print('nofcontrols: ${nofcontrols}');
  titles.add(DataColumn(label: Text('Nimi')));
  titles.add(DataColumn(label: Text('Tulos [min.sek]')));
  for (int i = 0; i < nofcontrols; i++) {
    titles.add(DataColumn(label: Text(i.toString())));
  }

  data['runners'].forEach((value) {
    List<DataCell> row = [];
    runner = value.toString().replaceAll('.', '-');
    row.add(DataCell(Text(runner)));
    print('runner: ${runner}');

    times = data[runner];
    print(' times: ${times}');
    if (times.length == nofcontrols) {
      int sec = times.last.seconds - times.first.seconds;
      row.add(DataCell(Text(to_min_sec(sec))));
      for (var se in times) {
        int sFromStart = se.seconds - times.first.seconds;
        row.add(DataCell(Text(to_min_sec(sFromStart))));
      }
    }
    else {
      row.add(DataCell(Text("DNF")));
      for (var se in times) {
        var sFromStart = se - times.first.seconds;
        row.add(DataCell(Text(to_min_sec(sFromStart))));
      }
    }
  results.add(DataRow(cells: row));
  });
  return DataTable(
    columns: titles,
    rows: results
    );
}

class ShowResults extends StatefulWidget {
  final String courseName;
  final String userName;
  ShowResults({this.courseName = null, this.userName = null});
  static const String route = 'show_results';


  @override
  _ShowResultsState createState() => _ShowResultsState();
}

class _ShowResultsState extends State<ShowResults> {


  @override
  Widget build(BuildContext context) {
    final String args = ModalRoute.of(context).settings.arguments;

    return Scaffold(
      appBar: AppBar(
        title: Text("Tulokset:"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder(
          //future: fetchTulos(args),
          future: _collectionRef.doc(args).get(),

        builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              print("has data!");
              print(snapshot.data.data());
              var data = fetchTulos(snapshot.data.data());
              if (data == null){
                return CircularProgressIndicator();
              }
              return SizedBox(
                width: double.infinity,
                child: data);
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }
}


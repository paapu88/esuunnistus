// a single orienteering course
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:flutter/services.dart';
import 'package:flutter_beep/flutter_beep.dart';
import 'package:vibration/vibration.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class Course {
  String courseName;
  String runnerName;
  List<LatLng> controls;
  List<CircleMarker> controlcircles;

  var punchTimes = <DateTime>[];
  int currentControl = 0;
  Distance distance = new Distance();

  Course({this.courseName, this.runnerName, this.controls, this.controlcircles});

  Future<bool> check_punch({@required LatLng latLon, double minDist = 20.0}) async{
    // check if we are punching
    print("controls: " + this.controls.length.toString());
    if (currentControl >= this.controls.length) {
      return true;
    }
    var latLonControl = this.controls[currentControl];
    double d = distance(latLon, latLonControl);
    print("Distance to next control:" + d.toString());
    if (d < minDist) {
      Vibration.vibrate(pattern: [500, 1000, 500, 2000], intensities: [255,255]);
      //Vibration.vibrate();
      //FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ALERT_AUTOREDIAL_LITE);
      //FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ALERT_CALL_GUARD);
      //FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ALERT_INCALL_LITE);
      FlutterBeep.playSysSound(AndroidSoundIDs.TONE_CDMA_ALERT_NETWORK_LITE);

      //FlutterBeep.beep();
      currentControl += 1;
      punchTimes.add(DateTime.now());
      if (currentControl == 1) {
        await this.updateUser();
      }
      await this.updatePunches();
      print("current control "+currentControl.toString());
      return true;
    }
    return false;
  }

  bool check_finish() {
    // check if we have punched start, all controls and finish
    if (currentControl >= this.controls.length) {
      return true;
    }
    return false;
  }


  Future<void> updateUser() async {
    // add user to persons who have run the course
    print('UPDATING DB ');
    print(this.courseName);
    print(this.runnerName);
    FirebaseFirestore.instance
        .collection('courses')
        .doc(this.courseName)
        .update(
      {
        'runners': FieldValue.arrayUnion([this.runnerName]),
      },
    );
  }

  Future<void> updatePunches() async {
    // update punches for a runner
    print('UPDATING PUNCH! ');
    print(this.courseName);
    print(this.runnerName);
    FirebaseFirestore.instance
        .collection('courses')
        .doc(this.courseName)
        .update(
      {
        this.runnerName.replaceAll('.', '-'): FieldValue.arrayUnion([this.punchTimes.last]),
      },
    );
  }


}
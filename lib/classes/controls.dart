import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;

class Piste {
  var piste = new List<CircleMarker>();

  Piste({double lat, double lon}) {
    piste.add(CircleMarker(
      //radius marker
      point: LatLng(lat, lon),
      color: Colors.yellow.withOpacity(1.0),
      borderStrokeWidth: 3.0,
      //useRadiusInMeter: true,
      //radius: 50, //radius
      //useRadiusInMeter: true,
      radius: 5, //radius
      borderColor: Colors.red,
    ));
    print(piste);
  }

  //factory Rastit.fromJson(Map<String, dynamic> json) {
  // return Rastit(
  //    track: json['track']
  //  );
  //}
}

class Rastit {
  final String track;
  var controls = new List<CircleMarker>();

  Rastit({this.track}) {
    var parts = new List<String>();
    parts = this.track.split(" ");
    //get iterator to the list
    var myListIter = parts.iterator;
    //iterate over the list
    while (myListIter.moveNext()) {
      String lats = myListIter.current.toString();
      double lat = double.parse(lats);
      myListIter.moveNext();
      String lons = myListIter.current.toString();
      double lon = double.parse(lons);
      controls.add(CircleMarker(
        //radius marker
        point: LatLng(lat, lon),
        color: Colors.blue.withOpacity(0.0),
        borderStrokeWidth: 3.0,
        //useRadiusInMeter: true,
        //radius: 50, //radius
        //useRadiusInMeter: true,
        radius: 10, //radius
        borderColor: Colors.red,
      ));
    }
    print(controls);
  }

  factory Rastit.fromJson(Map<String, dynamic> json) {
    return Rastit(track: json['track']);
  }
}

//Future<Rastit> fetchRastit({String url='https://map-and-db.herokuapp.com/track/', String name='espookr'}) async {
// final response = await http.get(url + name);
Future<Rastit> fetchRastit() async {
  final response =
      await http.get('https://map-and-db.herokuapp.com/track/espookr');
  //print(url + name);
  if (response.statusCode == 201) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print("RESPONSE a FINE !!");
    return Rastit.fromJson(json.decode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load Rastit' + response.statusCode.toString());
  }
}

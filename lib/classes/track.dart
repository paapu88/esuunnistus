import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:permission_handler/permission_handler.dart';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:gpx/gpx.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

//import 'package:file_picker/file_picker.dart';


class Track {
  //final String courseName;
  var trackList = new List<LatLng>();
  var controls = new List<CircleMarker>();
  var courseName;

  Track(String courseName) {
    this.trackList = [];
    this.controls = [];
    this.courseName=courseName;
  }
  void add_to_track(LatLng point){
    this.trackList.add(point);
  }
  void add_marker(LatLng point){
      this.controls.add(CircleMarker(
      //radius marker
      point: point,
      color: Colors.blue.withOpacity(0.0),
      borderStrokeWidth: 3.0,
      //useRadiusInMeter: true,
      //radius: 50, //radius
      //useRadiusInMeter: true,
      radius: 10, //radius
      borderColor: Colors.red,
    ));
  }
  void add_last_marker(LatLng point){
    this.controls.add(CircleMarker(
      //radius marker
      point: point,
      color: Colors.blue.withOpacity(0.0),
      borderStrokeWidth: 3.0,
      //useRadiusInMeter: true,
      //radius: 50, //radius
      //useRadiusInMeter: true,
      radius: 20, //radius
      borderColor: Colors.red,
    ));
  }
}

Future<Track> fetchTrack(String courseName) async {

  Track track=new Track(courseName);

  print("in fetch track");
  print(courseName);

  var lats;
  var lons;
  FirebaseFirestore.instance
      .collection('courses')
      .doc(courseName)
      .get()
      .then((DocumentSnapshot documentSnapshot) {
    if (documentSnapshot.exists) {
      print('Document data: ${documentSnapshot.data()}');
      print('Document data lats: ${documentSnapshot.get(FieldPath(['lats']))}');
      lats = documentSnapshot.get(FieldPath(['lats']));
      lons = documentSnapshot.get(FieldPath(['lons']));
      for (int i = 0; i < lats.length; i++) {
        print ("${lats[i]}");
        print ("${lons[i]}");
        track.add_to_track(LatLng(lats[i], lons[i]));
        track.add_marker(LatLng(lats[i], lons[i]));
      }
      track.add_last_marker(LatLng(lats[lats.length-1], lons[lats.length-1]));
    } else {
      print('Document does not exist on the database');
    }
  });
  return track;
}



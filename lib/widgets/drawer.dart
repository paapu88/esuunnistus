import 'package:flutter/material.dart';

import '../pages/map_ant.dart';
import '../pages/course_selection.dart';


Drawer buildDrawer(BuildContext context, String currentRoute) {
  return Drawer(
    child: ListView(
      children: <Widget>[
        const DrawerHeader(
          child: Center(
            child: Text('Flutter Map Examples'),
          ),
        ),
        ListTile(
          title: const Text('Valitse Rata'),
          selected: currentRoute == CourseSelection.route,
          onTap: () {
            Navigator.pushReplacementNamed(context, CourseSelection.route);
          },
        ),
        
      ],
    ),
  );
}

# esuunnistus (google play)

- Kirjaudu sisään google tunnuksilla. Tuloksissa näkyy google mailisi.
- Valitse listasta rata.
- Kannattaa valita ennen lähtöä sopiva mittakaava kahden sormen avulla. Painamalla pitkään pääset katsomaan rataa kokonaisuutena. Painamalla uudestaan pitkään pääset takaisin automaattiseen keskitys ja pyöritystilaan.
- Kello lähtee käyntiin kun pääset K:lle (lähtöpiste), merkitty ympyrällä.
- Oma sijainitisi näkyy pallukkana.
- Lähdössä, rasteilla ja maalissa (tuplaympyrä) leimaus tapahtuu kun känny värisee ja päästää ääntä.
- Lopputulokset löytyvät käynnistämällä sovellus uudelleen ja valitsemalla "Näytä tulokset". 

# uusi rata

- tee rata karttapullautin.fi
- lataa se omalle koneellesi (alla hekemistoon mapAndDB/trackfiles)
- lähetä pilveen (koneella pitää olla python asennettuna,
ja jos tulee valituksia puutteosta niin asenna python kirjastoja: pip install kirjasto:
```
wget https://raw.githubusercontent.com/paapu88/mapAndDB/master/gpxFile2Firebase.py
python gpxFile2Firebase.py ~/Downloads/mattby1.gps --name mattby

```

